<!DOCTYPE html>
<!--update language!-->
<html lang="nl">

<partial src="_head.html"/>
<!--make changes here-->
<title>Privacy Respecting and Fun WordPress.org Plugins on Our Old Website</title>
<meta name="description" content="Check out our old website built on WordPress.org and our favourite privacy respecting plugins we used. See how far we've come!">
<!--open graph meta tags-->
<meta property="og:type" content="article">
<meta property="og:url" content="https://gentlelivingshop.org/old-website/">
<meta property="og:title" content="Privacy Respecting and Fun WordPress.org Plugins on Our Old Website">
<meta property="og:description" content="Check out our old website built on WordPress.org and our favourite privacy respecting plugins we used. See how far we've come!">
<meta property="og:image" content="https://gentlelivingshop.org/resources/img/old-website-feature.webp">
<!--this page in another language-->
<link rel="alternate" href="https://gentlelivingshop.org/old-website.html" hreflang="en">
</head>

<body>

  <partial src="_header-nl-post.html"/>

  <!--main content starts-->
  <main>
    <h1>Privacy Respecting and Fun WordPress.org Plugins on Our Old Website</h1>
    <hr>
    <img class="post-thumbnail img-border" src="../resources/img/old-website-feature.webp" alt="our old website made with wordpress, post feature photo">
    <p>This website is coded by us using HTML and CSS. But before we knew how to do that, we started with WordPress.org using the <a href="https://wordpress.org/support/article/twenty-twenty-one/" target="_blank">Twenty Twenty-One theme</a>. It is a nice and simple theme, our favourite part is the dark mode. We wanted to build a website that is privacy respecting without having to display a cookie notice. The plugins we mention will therefore comply with GDPR policies.</p>
    <nav class="toc yellow">
      <h2>Jump to:</h2>
      <div class="toc-list">
        <ol>
          <li><a href="#plugin">WordPress plugins we liked</a></li>
          <li><a href="#migrate">Build your website <strong>without WordPress</strong>!</a></li>
          <li><a href="#oldwebsite">Old website's front page</a></li>
          <li><a href="#oldpost">Post page on the old website</a></li>
        </ol>
      </div>
    </nav>
    <hr class="hr-dotted">
    <div class="anchor" id="plugin"></div>
    <h2>WordPress.org plugins we liked</h2>
    <div class="iframe-container-16-9">
<iframe class="responsive-iframe" width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="Privacy Respecting and Fun WordPress.org Plugins (no cookie notice needed &amp; GDPR compliant!)" src="https://diode.zone/videos/embed/d9f9310f-5e75-4b07-ad9b-64170f7a8caf?subtitle=en" frameborder="0" allowfullscreen></iframe>
    </div>
    <p class="text-center"><a href="https://diode.zone/w/sV9ofWHBnz5tBJneMQZx3p?subtitle=en">open video on PeerTube</a></p>
    <p>You can also watch this video <a href="https://youtu.be/xjACQkUc2MQ" target="_blank">on YouTube</a>.</p>
    <hr class="hr-sun halfwidth">
    <ul class="text-justify">
      <li><a href="https://wordpress.org/plugins/polylang/" target="_blank">Polylang</a> - for making the website multilingual. It is easy to use but it sets a cookie that can be hard to remove. You need to go into the plugin php file to comment out some parts.</li>
      <li><a href="https://wordpress.org/plugins/dinosaur-game/" target="_blank">Dinosaur Game</a> - for the 404 page :)</li>
      <li><a href="https://wordpress.org/plugins/embed-privacy/" target="_blank">Embed Privacy</a> - prevent external embeds from loading without visiters' consent</li>
      <li><a href="https://wordpress.org/plugins/matomo/" target="_blank">Matomo Analytics</a> - ethical analytics. It was fun to see where our visitors come from in the world but we decided to not use any analytics anymore on this current site because it doesn't really align with our beliefs of not being tracked on the web.</li>
      <li><a href="https://wordpress.org/plugins/menu-icons/" target="_blank">Menu Icons</a> - fun icons for menus</li>
      <li><a href="https://wordpress.org/plugins/remove-ip/" target="_blank">Remove IP</a> - a simple plugin that stops the recording of IPs from comments</li>
      <li><a href="https://wordpress.org/plugins/wps-hide-login/" target="_blank">WP Hide Login</a> - change WP admin login url to prevent brute force attacks</li>
      <li><a href="https://wordpress.org/plugins/wp-2fa/" target="_blank">WP 2FA</a> - two-factor authentication for WP</li>
      <li><a href="https://wordpress.org/plugins/randompost/" target="_blank">randomPost</a> - click to display a random post</li>
      <li><a href="https://wordpress.org/plugins/wpfront-scroll-top/" target="_blank">WPFront Scroll Top</a> - add a scroll to top button</li>
      <li><a href="https://wordpress.org/plugins/wp-downgrade/" target="_blank">WP Downgrade</a> - rollback to old WP versions. This was especially helpful when WordPress updated to version 5.8 with new widget management which messed with our multilingual footer widgets. This plugin helped to revert WP back to the old version and restored the website.</li>
    </ul>
    <div class="flex-center">
      <hr class="hr-dotted">
      <div class="anchor" id="migrate"></div>
      <h2>Interested in building your own website (without WordPress)?</h2>
      <p>Tired of all the plugins? Want to have control over all the elements of your website? <a href="#">Read about how we made this website from scratch</a> going from not knowing how to write HTML or CSS to now!</p>
      <h3>Plugins to migrate away from WordPress</h3>
      <ul>
        <li><a href="https://wordpress.org/plugins/export-media-library/">Export Media Library</a> - download all photos from your website with a click</li>
        <li><a href="https://wordpress.org/plugins/simply-static/">Simply Static</a> - allows you to download the static code of your website for easy migration</li>
        <li><a href="https://wordpress.org/plugins/redirection/">Redirection</a> - redirect all traffic from you old wordpress website to your new website! Add custom links and manage redirection with ease.</li>
      </ul>
      <hr class="hr-dotted">
      <div class="anchor" id="oldwebsite"></div>
      <h2>Old website's front page</h2>
      <div class="container-overflow img-border">
        <img class="img-responsive" alt="screenshot of the old website front page" src="../resources/img/old-website-frontpage.webp" width="1272" height="5358">
      </div>
      <hr class="hr-dotted">
      <div class="anchor" id="oldpost"></div>
      <h2>Post page on the old website</h2>
      <div class="container-overflow">
        <img class="img-responsive" alt="screenshot of a post page on the old website" src="../resources/img/old-website-post.webp" width="1272" height="13514">
      </div>
    </div><hr class="hr-dotted">

    <h2 class="text-center">Thank you for reading!</h2>
  </main>
  <!--main content ends-->

  <hr>

  <partial src="_footer-nl.html"/>

</body>

</html>
